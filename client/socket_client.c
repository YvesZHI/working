#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <semaphore.h>
#include <signal.h>
#include <pthread.h>

#ifdef __APPLE__
#include <arpa/inet.h>
#endif

#define BUF 200

int sockclient;
int isTyping = 0;
char msgSent[BUF];
char msgRecv[BUF][BUF];
int cnt = 0;
pthread_mutex_t mtxType = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mtxRecvAndPrint = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condType = PTHREAD_COND_INITIALIZER;


void * threadSendMsg(void * arg)
{
	bzero(msgSent, BUF);
	fgets(msgSent, BUF, stdin);
	write(sockclient, msgSent, sizeof(msgSent));
	isTyping = 0;
	return NULL;
}

void enterMsg(int n)
{
	isTyping = 1;
	pthread_t pt;
	if(pthread_create(&pt, NULL, threadSendMsg, NULL) == -1)
	{
		printf("erreur de creation de thread\n");
		return;
	}
}

void * threadRecvMsg(void * arg)
{
	while(1)
	{
		if(read(sockclient, msgRecv[cnt], sizeof(msgRecv[cnt])) <= 0)
		{
			break;
		}
		cnt++;
	}
	return NULL;
}



int main(int argc,char** argv)
{
	signal(SIGINT, enterMsg);
	struct sockaddr_in coord_serveur;
	char * addrServer = NULL;
	u_short port;
	if(argc == 1)
	{
		addrServer = "127.0.0.1";
		port = 2000;
	}
	else if(argc == 3)
	{
		addrServer = argv[1];	
		port=atoi(argv[2]);
	}
	else
	{
		printf("Usage: ./client <adresse ip> <port>\n");
		return 1;
	}
	sockclient=socket(AF_INET, SOCK_STREAM, 0);
	bzero(&coord_serveur,sizeof(coord_serveur));
	coord_serveur.sin_family = AF_INET;
	coord_serveur.sin_port = htons(port);
	coord_serveur.sin_addr.s_addr = inet_addr(addrServer);
	if(connect(sockclient, (struct sockaddr *)&coord_serveur, sizeof(coord_serveur)) <0) 
	{ 
		printf ("erreur de connexion \n");
		exit(0);
	}
	for(int i = 0; i < BUF; i++)
	{
		bzero(msgRecv[i], BUF);
	}
	pthread_t pt, pt2;
	if(pthread_create(&pt, NULL, threadRecvMsg, NULL) == -1)
	{
		printf("erreur de creation de thread\n");
		return 1;
	}
	while(1)
	{
		for(int i = 0; i < cnt; i++)
		{
			printf("Message recu: %s\n", msgRecv[i]);
			bzero(msgRecv[i], BUF);
		}
		cnt = 0;
	}

	close(sockclient);

	return 0;
}

