#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

int main()
{
	/* Déclaration des variables */
	int sockserveur,newsockfd,lg;
	struct sockaddr_in coord_client;
	struct sockaddr_in mes_coord;
	struct tm *m;
	char* pc;
	char msg[100];
	/* Création d'un socket */
	sockserveur=socket(AF_INET, SOCK_STREAM, 0);
	/* Serveur : appel BIND */
	bzero(&mes_coord,sizeof(mes_coord));
	mes_coord.sin_family=AF_INET;
	mes_coord.sin_port=htons(2000);
	mes_coord.sin_addr.s_addr=htonl(INADDR_ANY);
	if(bind(sockserveur,(struct sockaddr*)&mes_coord,sizeof(mes_coord))<0){
		printf("Erreur BIND\n");
		exit(0);
	}
	/* Serveur : appel LISTEN */
	if (listen(sockserveur,5)<0){
		printf("Erreur LISTEN\n");
		exit(0);
	}
	/* Serveur : appel ACCEPT */
	while(1){
		lg=sizeof(coord_client);
		newsockfd=accept(sockserveur,(struct sockaddr*)&coord_client,(socklen_t *)&lg);
		if(newsockfd==(-1))
		{
			printf("Erreur ACCEPT\n");
			exit(0);
		}
		//lire la socket
		//read(newsockfd,msg,sizeof(msg));
		//printf(" message recut %s \n",msg);
		char msg2[100];
		while(1)
		{
			scanf("%s",msg2);
			write(newsockfd,msg2,sizeof(msg2));
			printf(" message envoye \n");
		}
		close(newsockfd);
	}
	close(sockserveur);
}

